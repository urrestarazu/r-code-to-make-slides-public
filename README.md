# r-code-to-make-slides-public

## Motivation
In order to view html files through my website with gitlab the files have to be in a "public" folder. So far, indivual .qmd files cannot be rendered into a "public" folder. This means they have to be moved to that folder. This codes aims at programatically moving them to a "public" folder.

## The code

The code in this .R file assumes:

The slides and associated files you want to make public are either in the wd, or in folder named "_extensions", "assets" or "_files" folder. For other configurations, add the code the folders to the filtering functions.

How it works:

1. It takes the folders in the wd and and the folders in them and clonates them in a folder named "public".

2. It copies the files to the "public" folder keeping the folder structure.

## Future steps


- [x] Try sourcing the document.

Access it here: https://gitlab.com/urrestarazu/r-code-to-make-slides-public/-/raw/main/make-html-files-public.R

- [ ] Prevent it from creating a "publicNA" folder

- [ ] Improve the code to check what has been re-rendered from the .qmd and only copy the updated files.

- [ ] Add code to make a gitlab .yaml that allows for the ci.

- [ ] Make a proper R function from it?

```
cd existing_repo
git remote add origin https://gitlab.com/urrestarazu/r-code-to-make-slides-public.git
git branch -M main
git push -uf origin main
```
